# Report

##  Name: Clothes & Footware Warehouse

### Author: Sabrina Ismailova

| ACTIONS                      | Start Date | End Date    |
|------------------------------|------------|-------------|
| Get Info about the Project   | 02.05.2023 | 02.05.2023  |
| Create Project               | 02.05.2023 | 03.05.2023  |
| Create Entity Design         | 03.05.2023 | 03.05.2023  |
| Optimize code Entity Design  | 04.05.2023 | 06.05.202   |
| Create Data Access Layer     | 06.05.2023 | 06.05.2023  |
| Optimize Data Access Layer   | 07.05.2023 | 10.05.2023  |
| Create Service Layer         | 10.05.2023 | 10.05.2023  |
| Optimize code Service Layer  | 11.05.2023 | 12.05.2023  |
| Create Controller Layer      | 12.05.2023 | 13.05.2023  |
| Optimize Controller Layer    | 14.05.2023 | 19.05.2023  |
| Create Repository on Gitlab  | 23.05.2023 | 23.05.2023  |
| Push codes on Repository     | 23.05.2023 | 23.05.2023  |     
| Final Testing                | 25.05.2023 | 26.05.2023  |        
| Create & edit Readme & Report| 03.06.2023 | 05.06.2023  |  
| Prepare the Presantation     | 05.06.2023 | 05.06.2023  |  
| End Project                  | 05.06.2023 | 05.06.2023  
