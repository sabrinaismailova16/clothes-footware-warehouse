import com.company.dto.ShoeDTO;
import com.company.dto.DataDTO;
import com.company.dto.EntityDTO;
import com.company.service.ShoeService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

public class ShoeServiceTest {

    private final ShoeService service = new ShoeService();

    @Test
    public void findByAllTest() {
        String sort = "1";
        EntityDTO<DataDTO<List<ShoeDTO>>> all = service.findAll(sort);
        Assertions.assertTrue(all.getData().isSuccess(), "Find all method is not passed!");
    }

    @Test
    public void findByIDTest() {
        Long id = 1L;
        EntityDTO<DataDTO<ShoeDTO>> responseEntity = service.findByID(id);
        Assertions.assertTrue(responseEntity.getData().isSuccess(), "Find by id method is not passed!");
    }

    @Test
    public void findByColorTest() {
        String color = "white";
        EntityDTO<DataDTO<List<ShoeDTO>>> responseEntity = service.findByColor(color);
        Assertions.assertEquals(responseEntity.getStatus(), 200, "Find by color method is not passed!");
    }

    @Test
    public void findBySizeTest() {
        Double size = 52D;
        EntityDTO<DataDTO<List<ShoeDTO>>> responseEntity = service.findBySize(size);
        Assertions.assertEquals(responseEntity.getStatus(), 200, "Find by size method is not passed!");
    }

    @Test
    public void findByOwnerTest() {
        String owner = "men";
        EntityDTO<DataDTO<List<ShoeDTO>>> responseEntity = service.findByOwner(owner);
        Assertions.assertEquals(responseEntity.getStatus(), 200, "Find by owner method is not passed!");
    }

    @Test
    public void filterByPriceTest() {
        Double max = 100D;
        Double min = 50D;
        EntityDTO<DataDTO<List<ShoeDTO>>> all = service.filterByPrice(min, max);
        Assertions.assertTrue(all.getData().isSuccess(), "Filter by price method is not passed!!");
    }

    @Test
    public void findByTypeTest() {
        String type = "sandals";
        EntityDTO<DataDTO<List<ShoeDTO>>> responseEntity = service.findByType(type);
        Assertions.assertEquals(responseEntity.getStatus(), 200, "Find by type method is not passed!");
    }
}
