package com.company.entity;

import com.company.config.FilePathEnum;
import com.company.service.ReaderService;
import com.company.dto.FormalDTO;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FormalWearEntity implements Entity<FormalDTO> {

    @Override
    public List<FormalDTO> get() throws IOException {
        return readFormalWearFile();
    }

    public List<FormalDTO> readFormalWearFile() throws IOException {
        List<FormalDTO> formalWearDTOS = new ArrayList<>();
        List<String> strings = ReaderService.file(FilePathEnum.formalwear.path());
        strings.forEach(s -> formalWearDTOS.add(toFormalWear(s)));
        return formalWearDTOS;
    }

    private FormalDTO toFormalWear(String line) {
        String[] strings = line.split(",");
        return FormalDTO.childBuilder()
                .id(Long.valueOf(strings[0]))
                .color(strings[1])
                .size(Double.valueOf(strings[2]))
                .owner(strings[3])
                .material(strings[4])
                .price(Double.valueOf(strings[5]))
                .quantity(Integer.valueOf(strings[6]))
                .company(strings[7])
                .job(strings[8])
                .build();
    }
}
