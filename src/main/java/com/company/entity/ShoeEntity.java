package com.company.entity;

import com.company.config.FilePathEnum;
import com.company.service.ReaderService;
import com.company.dto.ShoeDTO;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ShoeEntity implements Entity<ShoeDTO> {
    @Override
    public List<ShoeDTO> get() throws IOException {
        return readShoeFile();
    }

    public List<ShoeDTO> readShoeFile() throws IOException {
        List<ShoeDTO> shoes = new ArrayList<>();
        List<String> strings = ReaderService.file(FilePathEnum.shoe.path());
        strings.forEach(s -> shoes.add(toShoe(s)));
        return shoes;
    }

    private ShoeDTO toShoe(String line) {
        String[] strings = line.split(",");
        return ShoeDTO.childBuilder()
                .id(Long.valueOf(strings[0]))
                .color(strings[1])
                .size(Double.valueOf(strings[2]))
                .owner(strings[3])
                .material(strings[4])
                .price(Double.valueOf(strings[5]))
                .quantity(Integer.valueOf(strings[6]))
                .width(Double.valueOf(strings[7]))
                .length(Double.valueOf(strings[8]))
                .type(strings[9])
                .build();
    }
}
