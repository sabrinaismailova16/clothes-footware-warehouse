package com.company.entity;

import com.company.config.FilePathEnum;
import com.company.service.ReaderService;
import com.company.dto.TraditionalDTO;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TraditionalEntity implements Entity<TraditionalDTO> {

    @Override
    public List<TraditionalDTO> get() throws IOException {
        return readTraditionalWearFile();
    }

    public List<TraditionalDTO> readTraditionalWearFile() throws IOException {
        List<TraditionalDTO> traditionWears = new ArrayList<>();
        List<String> strings = ReaderService.file(FilePathEnum.traditionalwear.path());
        strings.forEach(s -> traditionWears.add(toTraditionalWear(s)));
        return traditionWears;
    }

    private TraditionalDTO toTraditionalWear(String line) {
        String[] strings = line.split(",");
        return TraditionalDTO.childBuilder()
                .id(Long.valueOf(strings[0]))
                .color(strings[1])
                .size(Double.valueOf(strings[2]))
                .owner(strings[3])
                .material(strings[4])
                .price(Double.valueOf(strings[5]))
                .quantity(Integer.valueOf(strings[6]))
                .country(strings[7])
                .length(Double.valueOf(strings[8]))
                .build();
    }
}
