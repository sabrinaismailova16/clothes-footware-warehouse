package com.company.entity;

import java.io.IOException;
import java.util.List;

public interface Entity<T> {
    List<T> get() throws IOException;
}
