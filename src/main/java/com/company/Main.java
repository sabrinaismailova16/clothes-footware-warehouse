package com.company;

import com.company.ui.AppUI;
import com.company.utils.BaseUtils;

public class Main {
    static AppUI appUI = new AppUI();

    public static void main(String[] args) {
        BaseUtils.println("\n\n*************** Project: Clothes & Footware WareHouse *****************");
        BaseUtils.println("--------------- Sabrina Ismailova ---------------");
        BaseUtils.println("--------------- Email: sabrina_ismailova@student.itpu.uz ---------------");
        BaseUtils.println("--------------- Creation date: since 02/05/23 ---------------");
        appUI.run();
    }
}