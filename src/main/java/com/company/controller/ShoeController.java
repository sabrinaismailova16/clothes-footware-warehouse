package com.company.controller;

import com.company.dto.ShoeDTO;
import com.company.dto.DataDTO;
import com.company.dto.EntityDTO;
import com.company.service.ShoeService;
import com.company.utils.BaseUtils;

import java.util.List;

public class ShoeController extends WearController {

    public ShoeController(ShoeService service){
        super(service);
    }

    public void byType() {
        BaseUtils.print("Enter type: ");
        BaseUtils.print(BaseUtils.gson.toJson(service.findByType(BaseUtils.readText())));
    }
}
