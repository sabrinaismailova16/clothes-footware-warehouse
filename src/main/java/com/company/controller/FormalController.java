package com.company.controller;

import com.company.dto.FormalDTO;
import com.company.dto.DataDTO;
import com.company.dto.EntityDTO;
import com.company.service.FormalWearService;
import com.company.utils.BaseUtils;

import java.util.List;

public class FormalController extends WearController {

    public FormalController(FormalWearService formalWearService) {
        super(formalWearService);
    }

    public void byJob() {
        BaseUtils.print("Write job: ");
        BaseUtils.print(service.findByJob(BaseUtils.readText()).toString());
    }
}
