package com.company.controller;

import com.company.dto.DTO;
import com.company.dto.DataDTO;
import com.company.dto.EntityDTO;
import com.company.dto.TraditionalDTO;
import com.company.service.Service;
import com.company.utils.BaseUtils;

import java.util.List;

public abstract class WearController extends Controller{
    public WearController(Service<? extends DTO> service) {
        super(service);
    }
    @Override
    public void all(String sort) {
        BaseUtils.print(BaseUtils.gson.toJson(service.findAll(sort)));
    }

    @Override
    public void byId() {
        BaseUtils.print("Enter id: ");
        BaseUtils.print(BaseUtils.gson.toJson(service.findByID(BaseUtils.readLong())));
    }

    @Override
    public void byColor() {
        BaseUtils.print("Enter color: ");
        BaseUtils.print(BaseUtils.gson.toJson(service.findByColor(BaseUtils.readText())));
    }

    @Override
    public void bySize() {
        BaseUtils.print("Enter size: ");
        BaseUtils.print(BaseUtils.gson.toJson(service.findBySize(BaseUtils.readDouble())));
    }

    @Override
    public void byOwner() {
        BaseUtils.print("Enter owner: ");
        BaseUtils.print(BaseUtils.gson.toJson(service.findByOwner(BaseUtils.readText())));
    }

    @Override
    public void byPrice() {
        BaseUtils.print("Enter min: ");
        Double min = BaseUtils.readDouble();
        BaseUtils.print("Enter max: ");
        Double max = BaseUtils.readDouble();
        BaseUtils.print(BaseUtils.gson.toJson(service.filterByPrice(min, max)));
    }

    public void byCountry() {
        BaseUtils.print("Enter country: ");
        BaseUtils.print(BaseUtils.gson.toJson(service.findByCountry(BaseUtils.readText())));
    }
}
