package com.company.controller;

import com.company.dto.DTO;
import com.company.service.Service;

public abstract class Controller {

    protected final Service<? extends DTO> service;

    public Controller(Service<? extends DTO> service) {
        this.service = service;
    }

    public abstract void all(String sort);
    public abstract void byId();
    public abstract void byColor();
    public abstract void bySize();
    public abstract void byOwner();
    public abstract void byPrice();
}
