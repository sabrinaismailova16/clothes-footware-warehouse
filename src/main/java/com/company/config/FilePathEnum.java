package com.company.config;

public enum FilePathEnum {
    formalwear, shoe, traditionalwear;

    public String path() {
        return "./src/main/resources/" + name() + ".csv";
    }

}
