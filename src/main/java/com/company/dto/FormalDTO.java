package com.company.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FormalDTO extends DTO {
    private String company;
    private String job;

    @Builder(builderMethodName = "childBuilder")
    public FormalDTO(Long id, String color, Double size, String owner,
                     String material, Double price, Integer quantity, String company, String job) {
        super(id, color, size, owner, material, price, quantity);
        this.company = company;
        this.job = job;
    }
}
