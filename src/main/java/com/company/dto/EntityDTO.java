package com.company.dto;

import lombok.Getter;

@Getter
public class EntityDTO<T> {
    private final T data;
    private Integer status;

    public EntityDTO(T data) {
        this.data = data;
    }

    public EntityDTO(T data, Integer status) {
        this.data = data;
        this.status = status;
    }
}
