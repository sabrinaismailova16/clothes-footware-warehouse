package com.company.dto;

import lombok.Getter;

@Getter
public class DataDTO<T> {
    private T body;
    private final boolean success;
    private ErrorDTO errorDTO;

    public DataDTO(ErrorDTO errorDTO) {
        this.errorDTO = errorDTO;
        this.success = false;
    }

    public DataDTO(T body) {
        this.body = body;
        this.success = true;
    }

}
