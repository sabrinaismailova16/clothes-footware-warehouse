package com.company.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TraditionalDTO extends DTO {
    private String country;
    private Double length;

    @Builder(builderMethodName = "childBuilder")
    public TraditionalDTO(Long id, String color, Double size, String owner,
                          String material, Double price, Integer quantity, String country, Double length) {
        super(id, color, size, owner, material, price, quantity);
        this.country = country;
        this.length = length;
    }
}
