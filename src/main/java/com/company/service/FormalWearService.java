package com.company.service;

import com.company.entity.FormalWearEntity;
import com.company.dto.*;
import com.company.exceptions.NotFoundException;

import java.util.Comparator;
import java.util.List;

public class FormalWearService implements Service<FormalDTO> {

    private final FormalWearEntity dao = new FormalWearEntity();

    @Override
    public EntityDTO<DataDTO<List<FormalDTO>>> findAll(String sort) {
        try {
            List<FormalDTO> formalWearDTOS = dao.get();
            if (formalWearDTOS.isEmpty()) {
                throw new NotFoundException("FormalWears not found!");
            }
            switch (sort) {
                case "1" -> formalWearDTOS.sort(Comparator.comparing(FormalDTO::getId));
                case "2" -> formalWearDTOS.sort(Comparator.comparing(FormalDTO::getPrice));
            }
            return new EntityDTO<>(new DataDTO<>(formalWearDTOS));
        } catch (Exception e) {
            return new EntityDTO<>(new DataDTO<>(ErrorDTO.builder()
                    .friendlyMessage(e.getMessage())
                    .developerMessage(e.getMessage())
                    .build()), 400);
        }
    }

    @Override
    public EntityDTO<DataDTO<FormalDTO>> findByID(Long id) {
        try {
            FormalDTO formalWearDTO = dao.get().stream().filter(formalWear1 ->
                    formalWear1.getId().equals(id)).findFirst().orElse(null);
            if (formalWearDTO == null) {
                throw new NotFoundException("FormalWear not found!");
            }
            return new EntityDTO<>(new DataDTO<>(formalWearDTO), 200);
        } catch (Exception e) {
            return new EntityDTO<>(new DataDTO<>(ErrorDTO.builder()
                    .friendlyMessage(e.getMessage())
                    .developerMessage(e.getMessage())
                    .build()), 400);
        }
    }

    @Override
    public EntityDTO<DataDTO<List<FormalDTO>>> findByColor(String color) {
        try {
            List<FormalDTO> formalWearDTOS = dao.get().stream().filter(formalWear ->
                    formalWear.getColor().equals(color)).toList();
            if (formalWearDTOS.isEmpty()) {
                throw new NotFoundException("FormalWear not found!");
            }
            return new EntityDTO<>(new DataDTO<>(formalWearDTOS), 200);
        } catch (Exception e) {
            return new EntityDTO<>(new DataDTO<>(ErrorDTO.builder()
                    .friendlyMessage(e.getMessage())
                    .developerMessage(e.getMessage())
                    .build()), 400);
        }
    }

    @Override
    public EntityDTO<DataDTO<List<FormalDTO>>> findBySize(Double size) {
        try {
            List<FormalDTO> formalWearDTOS = dao.get().stream().filter(formalWear ->
                    formalWear.getSize().equals(size)).toList();
            if (formalWearDTOS.isEmpty()) {
                throw new NotFoundException("FormalWear not found!");
            }
            return new EntityDTO<>(new DataDTO<>(formalWearDTOS), 200);
        } catch (Exception e) {
            return new EntityDTO<>(new DataDTO<>(ErrorDTO.builder()
                    .friendlyMessage(e.getMessage())
                    .developerMessage(e.getMessage())
                    .build()), 400);
        }
    }

    @Override
    public EntityDTO<DataDTO<List<FormalDTO>>> findByOwner(String owner) {
        try {
            List<FormalDTO> formalWearDTOS = dao.get().stream().filter(formalWear ->
                    formalWear.getOwner().equals(owner)).toList();
            if (formalWearDTOS.isEmpty()) {
                throw new NotFoundException("FormalWear not found!");
            }
            return new EntityDTO<>(new DataDTO<>(formalWearDTOS), 200);
        } catch (Exception e) {
            return new EntityDTO<>(new DataDTO<>(ErrorDTO.builder()
                    .friendlyMessage(e.getMessage())
                    .developerMessage(e.getMessage())
                    .build()), 400);
        }
    }

    @Override
    public EntityDTO<DataDTO<List<FormalDTO>>> filterByPrice(Double min, Double max) {
        try {
            List<FormalDTO> formalWearDTOS = dao.get().stream().filter(formalWear ->
                    formalWear.getPrice() >= min && formalWear.getPrice() <= max).toList();
            if (formalWearDTOS.isEmpty()) {
                throw new NotFoundException("FormalWear not found!");
            }
            return new EntityDTO<>(new DataDTO<>(formalWearDTOS), 200);
        } catch (Exception e) {
            return new EntityDTO<>(new DataDTO<>(ErrorDTO.builder()
                    .friendlyMessage(e.getMessage())
                    .developerMessage(e.getMessage())
                    .build()), 400);
        }
    }

    @Override
    public EntityDTO<DataDTO<List<TraditionalDTO>>> findByCountry(String country) {
        return null;
    }

    public EntityDTO<DataDTO<List<FormalDTO>>> findByJob(String job) {
        try {
            List<FormalDTO> formalWearDTOS = dao.get().stream().filter(formalWear ->
                    formalWear.getJob().equals(job)).toList();
            if (formalWearDTOS.isEmpty()) {
                throw new NotFoundException("FormalWear not found!");
            }
            return new EntityDTO<>(new DataDTO<>(formalWearDTOS), 200);
        } catch (Exception e) {
            return new EntityDTO<>(new DataDTO<>(ErrorDTO.builder()
                    .friendlyMessage(e.getMessage())
                    .developerMessage(e.getMessage())
                    .build()), 400);
        }
    }

    @Override
    public EntityDTO<DataDTO<List<ShoeDTO>>> findByType(String type) {
        return null;
    }
}
