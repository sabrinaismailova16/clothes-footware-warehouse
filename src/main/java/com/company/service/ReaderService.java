package com.company.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class ReaderService {

    public static List<String> file(String path) throws IOException {
        BufferedReader br = Files.newBufferedReader(Paths.get(path));
        return br.lines().skip(1).toList();
    }
}
