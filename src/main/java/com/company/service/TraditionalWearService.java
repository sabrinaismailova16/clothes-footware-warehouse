package com.company.service;

import com.company.entity.TraditionalEntity;
import com.company.dto.*;
import com.company.exceptions.NotFoundException;

import java.util.Comparator;
import java.util.List;

public class TraditionalWearService implements Service<TraditionalDTO> {

    private final TraditionalEntity dao = new TraditionalEntity();

    @Override
    public EntityDTO<DataDTO<List<TraditionalDTO>>> findAll(String sort) {
        try {
            List<TraditionalDTO> traditionalWearDTOS = dao.get();
            if (traditionalWearDTOS.isEmpty()) {
                throw new NotFoundException("TraditionalWears not found!");
            }
            switch (sort) {
                case "1" -> traditionalWearDTOS.sort(Comparator.comparing(TraditionalDTO::getId));
                case "2" -> traditionalWearDTOS.sort(Comparator.comparing(TraditionalDTO::getPrice));
            }
            return new EntityDTO<>(new DataDTO<>(traditionalWearDTOS));
        } catch (Exception e) {
            return new EntityDTO<>(new DataDTO<>(ErrorDTO.builder()
                    .friendlyMessage(e.getMessage())
                    .developerMessage(e.getMessage())
                    .build()), 400);
        }
    }

    @Override
    public EntityDTO<DataDTO<TraditionalDTO>> findByID(Long id) {
        try {
            TraditionalDTO traditionalWearDTO = dao.get().stream().filter(traditionalWear1 ->
                    traditionalWear1.getId().equals(id)).findFirst().orElse(null);
            if (traditionalWearDTO == null) {
                throw new NotFoundException("TraditionalWear not found!");
            }
            return new EntityDTO<>(new DataDTO<>(traditionalWearDTO), 200);
        } catch (Exception e) {
            return new EntityDTO<>(new DataDTO<>(ErrorDTO.builder()
                    .friendlyMessage(e.getMessage())
                    .developerMessage(e.getMessage())
                    .build()), 400);
        }
    }

    @Override
    public EntityDTO<DataDTO<List<TraditionalDTO>>> findByColor(String color) {
        try {
            List<TraditionalDTO> traditionalWearDTOS = dao.get().stream().filter(traditionalWear ->
                    traditionalWear.getColor().equals(color)).toList();
            if (traditionalWearDTOS.isEmpty()) {
                throw new NotFoundException("TraditionalWear not found!");
            }
            return new EntityDTO<>(new DataDTO<>(traditionalWearDTOS), 200);
        } catch (Exception e) {
            return new EntityDTO<>(new DataDTO<>(ErrorDTO.builder()
                    .friendlyMessage(e.getMessage())
                    .developerMessage(e.getMessage())
                    .build()), 400);
        }
    }

    @Override
    public EntityDTO<DataDTO<List<TraditionalDTO>>> findBySize(Double size) {
        try {
            List<TraditionalDTO> traditionalWearDTOS = dao.get().stream().filter(traditionalWear ->
                    traditionalWear.getSize().equals(size)).toList();
            if (traditionalWearDTOS.isEmpty()) {
                throw new NotFoundException("TraditionalWear not found!");
            }
            return new EntityDTO<>(new DataDTO<>(traditionalWearDTOS), 200);
        } catch (Exception e) {
            return new EntityDTO<>(new DataDTO<>(ErrorDTO.builder()
                    .friendlyMessage(e.getMessage())
                    .developerMessage(e.getMessage())
                    .build()), 400);
        }
    }

    @Override
    public EntityDTO<DataDTO<List<TraditionalDTO>>> findByOwner(String owner) {
        try {
            List<TraditionalDTO> traditionalWearDTOS = dao.get().stream().filter(traditionalWear ->
                    traditionalWear.getOwner().equals(owner)).toList();
            if (traditionalWearDTOS.isEmpty()) {
                throw new NotFoundException("TraditionalWear not found!");
            }
            return new EntityDTO<>(new DataDTO<>(traditionalWearDTOS), 200);
        } catch (Exception e) {
            return new EntityDTO<>(new DataDTO<>(ErrorDTO.builder()
                    .friendlyMessage(e.getMessage())
                    .developerMessage(e.getMessage())
                    .build()), 400);
        }
    }

    @Override
    public EntityDTO<DataDTO<List<TraditionalDTO>>> filterByPrice(Double min, Double max) {
        try {
            List<TraditionalDTO> traditionalWearDTOS = dao.get().stream().filter(traditionalWear ->
                    traditionalWear.getPrice() >= min && traditionalWear.getPrice() <= max).toList();
            if (traditionalWearDTOS.isEmpty()) {
                throw new NotFoundException("TraditionalWear not found!");
            }
            return new EntityDTO<>(new DataDTO<>(traditionalWearDTOS), 200);
        } catch (Exception e) {
            return new EntityDTO<>(new DataDTO<>(ErrorDTO.builder()
                    .friendlyMessage(e.getMessage())
                    .developerMessage(e.getMessage())
                    .build()), 400);
        }
    }

    public EntityDTO<DataDTO<List<TraditionalDTO>>> findByCountry(String country) {
        try {
            List<TraditionalDTO> traditionalWearDTOS = dao.get().stream().filter(traditionalWear ->
                    traditionalWear.getCountry().equals(country)).toList();
            if (traditionalWearDTOS.isEmpty()) {
                throw new NotFoundException("TraditionalWear not found!");
            }
            return new EntityDTO<>(new DataDTO<>(traditionalWearDTOS), 200);
        } catch (Exception e) {
            return new EntityDTO<>(new DataDTO<>(ErrorDTO.builder()
                    .friendlyMessage(e.getMessage())
                    .developerMessage(e.getMessage())
                    .build()), 400);
        }
    }

    @Override
    public EntityDTO<DataDTO<List<FormalDTO>>> findByJob(String job) {
        return null;
    }

    @Override
    public EntityDTO<DataDTO<List<ShoeDTO>>> findByType(String type) {
        return null;
    }
}
