package com.company.service;

import com.company.entity.ShoeEntity;
import com.company.dto.*;
import com.company.exceptions.NotFoundException;

import java.util.Comparator;
import java.util.List;

public class ShoeService implements Service<ShoeDTO> {

    private final ShoeEntity dao = new ShoeEntity();

    @Override
    public EntityDTO<DataDTO<List<ShoeDTO>>> findAll(String sort) {
        try {
            List<ShoeDTO> shoes = dao.get();
            if (shoes.isEmpty()) {
                throw new NotFoundException("Shoes not found!");
            }
            switch (sort) {
                case "1" -> shoes.sort(Comparator.comparing(ShoeDTO::getId));
                case "2" -> shoes.sort(Comparator.comparing(ShoeDTO::getPrice));
            }
            return new EntityDTO<>(new DataDTO<>(shoes));
        } catch (Exception e) {
            return new EntityDTO<>(new DataDTO<>(ErrorDTO.builder()
                    .friendlyMessage(e.getMessage())
                    .developerMessage(e.getMessage())
                    .build()), 400);
        }
    }

    @Override
    public EntityDTO<DataDTO<ShoeDTO>> findByID(Long id) {
        try {
            ShoeDTO shoe = dao.get().stream().filter(shoe1 ->
                    shoe1.getId().equals(id)).findFirst().orElse(null);
            if (shoe == null) {
                throw new NotFoundException("Shoe not found!");
            }
            return new EntityDTO<>(new DataDTO<>(shoe), 200);
        } catch (Exception e) {
            return new EntityDTO<>(new DataDTO<>(ErrorDTO.builder()
                    .friendlyMessage(e.getMessage())
                    .developerMessage(e.getMessage())
                    .build()), 400);
        }
    }

    @Override
    public EntityDTO<DataDTO<List<ShoeDTO>>> findByColor(String color) {
        try {
            List<ShoeDTO> shoes = dao.get().stream().filter(shoe ->
                    shoe.getColor().equals(color)).toList();
            if (shoes.isEmpty()) {
                throw new NotFoundException("Shoe not found!");
            }
            return new EntityDTO<>(new DataDTO<>(shoes), 200);
        } catch (Exception e) {
            return new EntityDTO<>(new DataDTO<>(ErrorDTO.builder()
                    .friendlyMessage(e.getMessage())
                    .developerMessage(e.getMessage())
                    .build()), 400);
        }
    }

    @Override
    public EntityDTO<DataDTO<List<ShoeDTO>>> findBySize(Double size) {
        try {
            List<ShoeDTO> shoes = dao.get().stream().filter(shoe ->
                    shoe.getSize().equals(size)).toList();
            if (shoes.isEmpty()) {
                throw new NotFoundException("Shoe not found!");
            }
            return new EntityDTO<>(new DataDTO<>(shoes), 200);
        } catch (Exception e) {
            return new EntityDTO<>(new DataDTO<>(ErrorDTO.builder()
                    .friendlyMessage(e.getMessage())
                    .developerMessage(e.getMessage())
                    .build()), 400);
        }
    }

    @Override
    public EntityDTO<DataDTO<List<ShoeDTO>>> findByOwner(String owner) {
        try {
            List<ShoeDTO> shoes = dao.get().stream().filter(shoe ->
                    shoe.getOwner().equals(owner)).toList();
            if (shoes.isEmpty()) {
                throw new NotFoundException("Shoe not found!");
            }
            return new EntityDTO<>(new DataDTO<>(shoes), 200);
        } catch (Exception e) {
            return new EntityDTO<>(new DataDTO<>(ErrorDTO.builder()
                    .friendlyMessage(e.getMessage())
                    .developerMessage(e.getMessage())
                    .build()), 400);
        }
    }

    @Override
    public EntityDTO<DataDTO<List<ShoeDTO>>> filterByPrice(Double min, Double max) {
        try {
            List<ShoeDTO> shoes = dao.get().stream().filter(shoe ->
                    shoe.getPrice() >= min && shoe.getPrice() <= max).toList();
            if (shoes.isEmpty()) {
                throw new NotFoundException("Shoe not found!");
            }
            return new EntityDTO<>(new DataDTO<>(shoes), 200);
        } catch (Exception e) {
            return new EntityDTO<>(new DataDTO<>(ErrorDTO.builder()
                    .friendlyMessage(e.getMessage())
                    .developerMessage(e.getMessage())
                    .build()), 400);
        }
    }

    @Override
    public EntityDTO<DataDTO<List<TraditionalDTO>>> findByCountry(String country) {
        return null;
    }

    @Override
    public EntityDTO<DataDTO<List<FormalDTO>>> findByJob(String job) {
        return null;
    }

    public EntityDTO<DataDTO<List<ShoeDTO>>> findByType(String type) {
        try {
            List<ShoeDTO> shoes = dao.get().stream().filter(shoe ->
                    shoe.getType().equals(type)).toList();
            if (shoes.isEmpty()) {
                throw new NotFoundException("Shoe not found!");
            }
            return new EntityDTO<>(new DataDTO<>(shoes), 200);
        } catch (Exception e) {
            return new EntityDTO<>(new DataDTO<>(ErrorDTO.builder()
                    .friendlyMessage(e.getMessage())
                    .developerMessage(e.getMessage())
                    .build()), 400);
        }
    }
}
