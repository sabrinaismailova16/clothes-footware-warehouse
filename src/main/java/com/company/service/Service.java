package com.company.service;

import com.company.dto.*;

import java.util.List;

public interface Service<T> {
    EntityDTO<DataDTO<List<T>>> findAll(String sort);

    EntityDTO<DataDTO<T>> findByID(Long id);

    EntityDTO<DataDTO<List<T>>> findByColor(String color);

    EntityDTO<DataDTO<List<T>>> findBySize(Double size);

    EntityDTO<DataDTO<List<T>>> findByOwner(String owner);

    EntityDTO<DataDTO<List<T>>> filterByPrice(Double min, Double max);

    EntityDTO<DataDTO<List<TraditionalDTO>>> findByCountry(String country);
    EntityDTO<DataDTO<List<FormalDTO>>> findByJob(String job);
    EntityDTO<DataDTO<List<ShoeDTO>>> findByType(String type);
}
